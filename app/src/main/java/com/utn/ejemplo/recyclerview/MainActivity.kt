package com.utn.ejemplo.recyclerview

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView


class MainActivity : AppCompatActivity() {

    private var lista: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val lista: RecyclerView = findViewById(R.id.lista)

        // Los LayoutManagers deciden cómo usar el espacio disponible de un RecyclerView.
        // Distintas clases de LayoutManager van a mostrar los datos con distinta disposición,
        // bien como lista, como grilla, o como algo completamente distinto.
        // Aca mostramos como hacerlo en Kotlin pero ya esta hecho desde XML
        //lista.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        this.lista = lista
        llenarLista()
    }

    // Agrega contenido al RecyclerView
    private fun llenarLista() {
        val libros = obtenerLibros()
        lista!!.adapter = AdapterLibros(libros)
    }
}

package com.utn.ejemplo.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

/**
 * Los Adapters reciben datos puros (lo que se llama datos del negocio) y los convierten en Views de
 * Android que representan los mismos datos, para ser mostradas en el RecyclerView.
 *
 * Esos Views no se manipulan directamente sino a través de ViewHolders.
 * Los ViewHolders existen para ahorrar procesamiento, más adelante vamos a ver por qué.
 */
class AdapterLibros(val items: Array<Libro>): RecyclerView.Adapter<LibroVH>() {

    /**
     * Se invoca este método cuando se crea una vista nueva. Como las vistas se manejan a través de
     * ViewHolders, también se crea un ViewHolder, que engloba la vista.
     *
     * parent: Es el contenedor de la vista que estamos por crear. En nuestro caso es el RecyclerView.
     * viewType: en este ejemplo sólo mostramos vistas a partir de R.layout.item, pero en programas más
     *           complejos, podemos mostrar vistas de varios layouts distintos. Cada uno se identifica
     *           por un número, y ese número viene en este argumento para indicar cuál layout queremos inflar,
     *           y cuál tipo de ViewHolder tenemos que crear.
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LibroVH {
        // Crear una View a partir de un layout XML se denomina "inflar" el layout.
        // Los objetos que hacen eso se llaman LayoutInflaters, y cada Activity tiene uno.
        // La clase Activity hereda de la clase Context, y todas las vistas de una Activity tienen una propiedad context,
        // que apunta a esa Activity (en nuestro caso MainActivity).
        // El ViewGroup parent es una vista, entonces tiene una referencia a MainActivity.
        // Por eso, como LayoutInflater.from() necesita un Context para crear un LayoutInflater,
        // usamos el que provee parent.
        val inflater = LayoutInflater.from(parent.context)
        val vista = inflater.inflate(R.layout.item, parent, false)
        return LibroVH(vista)
    }

    // Vincula los datos puros con una View, a traves del ViewHolder
    // Otra forma de verlo: hace que una View represente los datos puros en la posicion i
    override fun onBindViewHolder(holder: LibroVH, position: Int) {
        holder.tituloTv.text = items[position].titulo
        holder.autorTv.text = items[position].autor
    }

    // Indica cuantos elementos debe mostrar el RecyclerView
    override fun getItemCount(): Int {
        return items.size
    }
}

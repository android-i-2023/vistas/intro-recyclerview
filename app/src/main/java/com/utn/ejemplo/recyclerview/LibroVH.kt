package com.utn.ejemplo.recyclerview

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

// Esta clase sólo toma una vista de libro (View) y le invoca findViewById para referenciar directamente
// las vistas de la foto, el titulo y el autor.
// Esto se crea en Adapter.onCreateViewHolder.
class LibroVH(vistaLibro: View) : RecyclerView.ViewHolder(vistaLibro) {
    val fotoIv: ImageView
    val tituloTv: TextView
    val autorTv: TextView

    // Cuerpo del constructor
    init {
        fotoIv = vistaLibro.findViewById(R.id.foto)
        tituloTv = vistaLibro.findViewById(R.id.titulo)
        autorTv = vistaLibro.findViewById(R.id.autor)
    }
}

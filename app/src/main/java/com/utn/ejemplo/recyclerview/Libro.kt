package com.utn.ejemplo.recyclerview

data class Libro(
    val titulo: String,
    val autor: String
)

// En un proyecto real los datos casi nunca van a estar harcodeados como éstos.
// Tampoco van a venir de una función, sino de un método de un objeto
// Ya nos vamos a ocupar de esto más adelante
// Varios de los libros de esta lista son clasicos de la programacion!
fun obtenerLibros(): Array<Libro> = arrayOf(
    Libro("El Principito", "Antoinne de Saint-Exupery"),
    Libro("El Gaucho Martin Fierro", "Jose Hernandez"),
    Libro("Sobre Heroes y Tumbas", "Ernesto Sabato"),
    Libro("Facundo", "Domingo F. Sarmiento"),
    Libro("El Aleph", "Jorge L. Borges"),
    Libro("El Lenguaje de Programación C", "Brian Kernighan & Dennis Ritchie"),
    Libro("El Lenguaje de Programación C++", "Bjarne Stroustrup"),
    Libro("Algoritmos", "Robert Sedgewick & Kevin Wayne"),
    Libro("El Arte de la Programación de Computadoras", "Donald Knuth"),
    Libro(
        "Estructura e Interpretación de Programas de Computadora",
        "Harold Abelson & Gerald Jay Sussman & Julie Sussman"
    ),
    Libro("Patrones de Arquitectura para Aplicaciones Corporativas", "Martin Fowler"),
    Libro(
        "Introducción a los Algoritmos",
        "Thomas Cormen & Charles Leiserson & Ronald Rivest & Cliffor Stein"
    ),
    Libro("No me hagas pensar", "Steve Krug"),
    Libro("Cracking the Coding Interview", "Gayle Laakmann McDowell"),
    Libro(
        "Patrones de Diseño",
        "Erich Gamma & Richard Helm & Ralph Johnson & John Vlissides"
    ),
    Libro("Trabajando Efectivamente con Código Legado", "Michael Feathers"),
    Libro("Refactorizando", "Martin Fowler"),
    Libro("Código Limpio", "Robert C. Martin"),
    Libro("El Programador Pragm'atico", "David Thomas & Andrew Hunt"),
)
